require "./vt"

module Tion
  extend self

  def bar
    bar = Bar.new
    yield bar
    bar.finish
  end

  class Bar
    def initialize(@width : UInt16 = VT.get_size.ws_col,
                   @prefix = "[",
                   @suffix = "]",
                   @complete = '▓',
                   @incomplete = '░',
                   @prc : Float64 = 0.to_f,
                   @out : IO::FileDescriptor = STDOUT)
      VT.hide_cursor

      @start = Time.now
    end

    property prc, width, prefix, suffix

    def print
      @out.print "\r" + bar
      @out.flush
    end

    def finish
      @out.puts
      VT.show_cursor
    end

    private def bar
      width = @width - @prefix.size - @suffix.size
      pos = (@prc.to_f * width.to_f).to_i

      prefix
        .+ (@complete.to_s * pos)
        .+ (@incomplete.to_s * (width - pos))
        .+ suffix
    end
  end
end
