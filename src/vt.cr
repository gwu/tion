module VT
  extend self

  lib C
    struct Winsize
      ws_row : UInt16    # rows, in characters
      ws_col : UInt16    # columns, in characters
      ws_xpixel : UInt16 # horizontal size, pixels
      ws_ypixel : UInt16 # vertical size, pixels
    end

    TIOCGWINSZ = 0x5413  # TERMINAL WINDOW SIZE

    fun ioctl(fd : Int32,
              request : UInt32,
              winsize : C::Winsize*
             ) : Int32
  end

  def get_size
    C.ioctl(0, C::TIOCGWINSZ, out screen_size)
    return screen_size
  end

  def hide_cursor
    STDOUT.print "\x1b[?25l"
  end

  def show_cursor
    STDOUT.print "\x1b[?25h"
  end
end
