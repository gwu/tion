require "./helper"

describe Tion do
  # bar = Tion::Bar.new
  # bar.prc = 0.1
  # bar.prc = 0.4
  # bar.print
  # sleep 0.5
  # bar.finish

  Tion.bar do |ctx|
    ctx.prefix = "a bar of this: ["
    ctx.prc = 0.1
    ctx.print
    sleep 0.5
    ctx.prc = 1.0
    ctx.print
    sleep 0.5
    ctx.prc = 0.4
    ctx.print
    sleep 0.5
    ctx.prc = 0.8
    ctx.print
    sleep 0.5
    ctx.prc = 1.0
    ctx.print
    sleep 0.5
  end
end
