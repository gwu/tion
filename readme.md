# tion

it's at least better than `progress [=.]`

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     tion:
       gitlab: gwu/tion
   ```

2. Run `shards install`

## Usage

```crystal
require "tion"
```

it's simple!

```crystal
bar = Tion.Bar.new
# the rest should be obvious :P
```

## Development

please help me expand something small like this.
i might try and write a tui library after i've gotten
a thourough understanding of vt

## Contributing

1. Fork it (<https://gitlab.com/gwu/tion/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [gwu](https://gitlab.com/gwu) - me. uwu
